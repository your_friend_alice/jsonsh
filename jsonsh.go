package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
)

func exitErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func file() (io.Reader, error) {
	if len(os.Args) == 2 {
		return os.Open(os.Args[1])
	} else if len(os.Args) == 1 {
		return os.Stdin, nil
	}
	return nil, fmt.Errorf("Usage: %s [script-file]", os.Args[0])
}

func consumeShebang(file io.Reader) (io.Reader, error) {
	// read the first 2 bytes. If they're "#!", it's a shebang so throw away the
	// rest of the line.
	startBytes := make([]byte, 2)
	_, err := file.Read(startBytes)
	exitErr(err)
	b := make([]byte, 1)
	if string(startBytes) == "#!" {
		// read till there's a \n
		for b[0] != '\n' {
			_, err := file.Read(b)
			if err != nil {
				return file, err
			}
		}
	} else {
		// we already read the first 2 bytes but they weren't a shebang. We can't
		// seek backwards on STDIN, so lets concatenate those back to the start of
		// the stream.
		file = io.MultiReader(bytes.NewReader(startBytes), file)
	}
	return file, nil
}

func run(args []string) error {
	if len(args) == 0 {
		return errors.New("Empty args array")
	}
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func main() {
	reader, err := file()
	exitErr(err)
	reader, err = consumeShebang(reader)
	exitErr(err)
	decoder := json.NewDecoder(reader)
	args := []string{}
	for {
		err := decoder.Decode(&args)
		if errors.Is(err, io.EOF) {
			break
		}
		run(args)
		exitErr(err)
	}
}
