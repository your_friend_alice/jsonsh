# `jsonsh`

_If you don't know why you would want this, that's a good thing, close this repo
and be thankful._

`jsonsh` is a simple shell, it just reads in JSON arrays and executes them as
commands. Reads commands from STDIN or a filename argument, and can be invoked
via shebang line.

## Why?

Imagine you're stuck in some language that only lets you run external commands
using *a shell*. Prepare for weird bugs at best, and remote code exec
vulnerabilities at worst. Good thing basically everything these days has a JSON
library! If worst comes to worst, here's a shell with almost no features, and
a simple, unambiguous escaping rules that your language probably already
implements in its standard library.

## Installation

```
go get gitlab.com/your_friend_alice/jsonsh
```

## Example

### `test.jsonsh`
```
#!jsonsh
["echo", "test1"]
["echo", "test2"]["echo", "test3"]
```

### Output
```
test1
test2
test3
```

## Pronunciation

Either "jay-sawn-shh" or "jay-sun-shh", depending on how you pronounce JSON.
